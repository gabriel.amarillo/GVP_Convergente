var version = "8.1.41";


/**
 * Template for Custom Prompt methods.
 * All Custom Prompts functions should start with Language locale prefix
 * e.g. for English Australia en-AU, custom prompt methods should be enAU$MethodName$
 * 
 * 'promptData' argument is auto forwarded by Composer Code gen, it would be value of the Prompt Value set in the Prompts Dialog. 
 * 'promptData' argument is must for all the Custom methods.
 * Custom arguments are user defined arguments.
*/



/**
Funcion que reproduce un Audio AMR, se le ingresa el ID del Audio de GAX
*/
function esARAudioGax(promptData)
{
	var resultArray = resultArray = [];
	
	 
	 
	resultArray.push(getAppVar('gvpSession').uriGax +'1'+ promptData +getAppVar('gvpSession').personalityId + "_pcma.wav");
	
	
	return resultArray;
}

function esARAudioGaxArray(promptData)


{
	
	              
var array  = promptData.split(',');



var resultArray = [];


for (var i= 0;i<array.length;i++) {
    
     
                
         resultArray.push(getAppVar('gvpSession').uriGax + '1'+ array[i] + getAppVar('gvpSession').personalityId + "_pcma.wav");
 	    
   			     
       
        }
   	 
		
	return resultArray;
}
/**
Funcion que reproduce  Audios AMR, se le ingresa el Nombre del Objeto JSON de MENU Dinamico
<menu xmlns="http://ws.wso2.org/dataservice" retries="9" retryPrompt="101100510">
   </option>
   <option>
         </prompt>
          <prompt>
            <porder>2</porder>
            <audio>101100710</audio>
         </prompt>
      </moprompt>
   </option>
</menu>

*/
function esARAmrArrayMenu(promptData)
{
var json =getAppVar(promptData);


var resultArray = [];
var prompt;

for (var i= 0;i<json.menu.option.length;i++) {
    
      prompt = json.menu.option[i].moprompt.prompt;
       
       if(isArray(prompt)) {
                 for (var j= 0 ;j<prompt.length ;j++) {
                  resultArray.push(getAppVar('AppCallData').uriGax +'101'+ prompt[j].audio + getAppVar('AppCallData').personalityID + "_pcmu.wav");
                 
                    }
   			 } 
   			 else {
   				
                
                  resultArray.push(getAppVar('AppCallData').uriGax + '101'+ prompt.audio + getAppVar('AppCallData').personalityID + "_pcmu.wav");
       		    
   			     }
 
        
        }
   	 
		
	return resultArray;
}

function esARAmrArray(promptData)
{
var json =getAppVar(promptData);



var resultArray = [];
var prompt;

for (var i= 0;i<json.length;i++) {
    
     
                
         resultArray.push(getAppVar('AppCallData').uriGax + '101'+ json[i] + getAppVar('AppCallData').personalityID + "_pcmu.wav");
 	    
   			     
       
        }
   	 
		
	return resultArray;
}


function  esARAmrComplexArray(audioArray)
{
	
    var array = getAppVar(audioArray);	
	var resultArray = [];
	var numericArray = [];
	var j;
	for (var i= 0;i<array.audioArray.audio.length;i++) {
	   
	   if ( array.audioArray.audio[i].type =="gax")
	     {
        resultArray.push(getAppVar('AppCallData').uriGax + '101'+ array.audioArray.audio[i].value + getAppVar('AppCallData').personalityID + "_pcmu.wav");
          }
          else if ( array.audioArray.audio[i].type =="number")
           {
               numericArray = alphanumericPrompts(array.audioArray.audio[i].value);
                for (var j= 0;j<numericArray.length;j++) {
                   resultArray.push(numericArray[j]);
  		             }
  		    }
          		
  		 
       }	
	   		
	return resultArray;
}	


function esARdecirAniDeADos(promptData)
{

return undefined;

}


